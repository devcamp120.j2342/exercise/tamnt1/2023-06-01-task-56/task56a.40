﻿package com.example.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.Service.CircleService;

//Khai báo là 1 controller
@RestController
// Khai báo tiền tố của api
@RequestMapping("/api")
// Khai báo phạm vi server có thể truy cập
@CrossOrigin
public class CircleController {
    @Autowired
    private CircleService service;

    @GetMapping("/circle-area")
    public void getEmployee(@RequestParam double radius) {
        service.createCircle(radius);
    }
}
