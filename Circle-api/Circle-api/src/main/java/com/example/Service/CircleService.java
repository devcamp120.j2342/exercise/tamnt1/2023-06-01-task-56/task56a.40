﻿package com.example.Service;

import org.springframework.stereotype.Service;

import com.example.Circle;

@Service
public class CircleService {

    public void createCircle(double radius) {
        Circle cirle = new Circle(radius);
        System.out.println(cirle);
    }
}
